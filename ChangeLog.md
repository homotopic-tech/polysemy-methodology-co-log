# Changelog for polysemy-methodology-co-log

## v0.1.0.0

Add `logMethodologyStart`, `logMethodologyEnd` and `logMethodologyAround`.
